from unittest import TestCase

from flatland.envs.rail_env import RailEnv

from libs.cell_graph import CellGraph
from libs.cell_graph_locker import CellGraphLocker

SEED = 42

WIDTH = 10
HEIGHT = 10


class TestCellGraphLocker(TestCase):
    def setUp(self):

        self.env = RailEnv(width=WIDTH,
                           height=HEIGHT,
                           random_seed=SEED
                           )
        self.env.reset()


        self.graph = CellGraph(self.env)
        self.locker = CellGraphLocker(self.graph)

    def tearDown(self):
        del self.locker
        del self.graph
        del self.env

    def test_not_empty(self):
        self.assertGreater(len(self.locker.data), 0)

    def test_lock_single_position_by_another_agent(self):
        self.locker.lock(0, 0, (0,2))

        self.assertTrue(self.locker.is_locked(0, 1, (0, 2)))
        self.assertTrue(self.locker.is_locked(0, 1, (1, 2)))
        self.assertTrue(self.locker.is_locked(0, 1, (-1, 1)))
        self.assertTrue(self.locker.is_locked(0, 1, (-1, 10)))

        self.assertFalse(self.locker.is_locked(0, 1, (2, 3)))
        self.assertFalse(self.locker.is_locked(0, 1, (3, 10)))
        self.assertFalse(self.locker.is_locked(0, 1, (10, 20)))
        self.assertFalse(self.locker.is_locked(0, 1, (-1, 0)))

    def test_lock_single_position_by_agent_itself(self):
        self.locker.lock(0, 0, (0,2))

        self.assertFalse(self.locker.is_locked(0, 0, (0, 2)))
        self.assertFalse(self.locker.is_locked(0, 0, (1, 2)))
        self.assertFalse(self.locker.is_locked(0, 0, (-1, 1)))
        self.assertFalse(self.locker.is_locked(0, 0, (-1, 10)))

        self.assertFalse(self.locker.is_locked(0, 0, (2, 3)))
        self.assertFalse(self.locker.is_locked(0, 0, (3, 10)))
        self.assertFalse(self.locker.is_locked(0, 0, (10, 20)))
        self.assertFalse(self.locker.is_locked(0, 0, (-1, 0)))


    def test_lock_agent_after_agent(self):
        self.locker.lock(0, 0, (0, 2))
        self.assertTrue(self.locker.is_locked(0, 1, (0, 2)))
        self.assertFalse(self.locker.is_locked(0, 1, (2, 5)))
        self.locker.lock(0, 1, (2, 5))

        self.assertTrue(self.locker.is_locked(0, 2, (0, 1)))
        self.assertTrue(self.locker.is_locked(0, 2, (3, 4)))
        self.assertTrue(self.locker.is_locked(0, 2, (2, 3)))
        self.assertTrue(self.locker.is_locked(0, 2, (2, 10)))
        self.assertTrue(self.locker.is_locked(0, 2, (-2, 10)))

        self.assertFalse(self.locker.is_locked(0, 0, (5, 7)))
        self.assertFalse(self.locker.is_locked(0, 0, (5, 10)))
        self.assertFalse(self.locker.is_locked(0, 0, (10, 20)))
        self.assertFalse(self.locker.is_locked(0, 0, (-1, 0)))


    def test_lock_agent_after_agent_with_extending(self):
        self.locker.lock(0, 0, (0, 2))
        self.assertTrue(self.locker.is_locked(0, 1, (0, 2)))
        self.assertFalse(self.locker.is_locked(0, 1, (5, 10)))
        self.locker.lock(0, 1, (5, 10))

        self.assertTrue(self.locker.is_locked(0, 2, (0, 1)))
        self.assertFalse(self.locker.is_locked(0, 2, (3, 4)))
        self.assertFalse(self.locker.is_locked(0, 2, (2, 3)))
        self.assertTrue(self.locker.is_locked(0, 2, (2, 10)))
        self.assertTrue(self.locker.is_locked(0, 2, (-2, 10)))

        # #extend
        # self.assertFalse(self.locker.is_locked(0, 1, (2, 10)))
        # self.locker.lock(0, 1, (2, 10))
        #
        # self.assertTrue(self.locker.is_locked(0, 2, (0, 1)))
        # self.assertTrue(self.locker.is_locked(0, 2, (3, 4)))
        # self.assertTrue(self.locker.is_locked(0, 2, (2, 3)))
        # self.assertTrue(self.locker.is_locked(0, 2, (2, 10)))
        # self.assertTrue(self.locker.is_locked(0, 2, (-2, 10)))
        #
        # self.assertFalse(self.locker.is_locked(0, 0, (10, 17)))
        # self.assertFalse(self.locker.is_locked(0, 0, (10, 11)))
        # self.assertFalse(self.locker.is_locked(0, 0, (20, 30)))
        # self.assertFalse(self.locker.is_locked(0, 0, (-1, 0)))


    def test_lock_assert_situation(self):
        self.locker.lock(15, 142, (252, 257))
        self.assertTrue(self.locker.is_locked(15, 17, (253, 258)))

        self.locker.reset()
        self.locker.lock(15, 142, (253, 255))
        self.assertTrue(self.locker.is_locked(15, 17, (253, 258)))
