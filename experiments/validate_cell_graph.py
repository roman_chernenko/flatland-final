from libs.cell_graph_validator import CellGraphValidator
from libs.cell_graph_dispatcher import CellGraphDispatcher

import numpy as np


def no_sort_dispatcher(env):
    return CellGraphDispatcher(env)


micro_test = {
    "width": [50],
    "height": [50],
    "cities": [10],
    "trains": [50],
    "seed": 42
}

mini_test = {
    "width": [50, 50, 100, 125],
    "height": [50, 50, 100, 125],
    "cities": [10]*4,
    "trains": [100, 200, 100, 100],
    "seed": 42
}

#small_test
N = 15
seed = 42

np.random.seed(seed)
width = np.random.randint(20, 150, (N,))
height = np.random.randint(20, 150, (N,))
width[N-1] = 150
height[N//2:] = width[N//2:]
nr_train = np.random.randint(50, 200, (N,))
nr_train[N-1] = 200
n_cities = np.random.randint(2, 35, (N,))
grid_distribution_of_cities = False
max_rails_between_cities = np.random.randint(2, 4, (N,))
max_rail_in_city = np.random.randint(3, 6, (N,))
malfunction_rate = np.random.randint(500, 4000, (N,))
prop_malfunction = np.random.uniform(0.01, 0.01, (N,))
min_duration = np.random.randint(20, 80, (N,))
max_duration = np.random.randint(20, 80, (N,))
max_duration = np.maximum(min_duration, max_duration)
small_test = {
    "width": width,
    "height": height,
    "trains": nr_train,
    "seed": seed,
    "cities": n_cities,
    "rails_between_cities": max_rails_between_cities,
    "rails_in_city": max_rail_in_city,
    "malfunction_rate": malfunction_rate,
    "prop_malfunction": prop_malfunction,
    "min_prop": min_duration,
    "max_prop": max_duration
}

#Run
CellGraphValidator.multiple_tests(no_sort_dispatcher, **mini_test)
