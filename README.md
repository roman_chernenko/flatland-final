# README #

Flatland Challenge 2nd place solution. 

https://www.aicrowd.com/challenges/flatland-challenge

AMLD 2020 talk about solution:

https://www.youtube.com/watch?v=rGzXsOC7qXg&t=12m04s

Presentation:

https://docs.google.com/presentation/d/12bbp7MwoB0S7FaTYI4QOAKMoijf_f4em64VkoUtdwts/edit?usp=sharing



### Authors ###

* Roman Chernenko
* Vitaly Bondar


### License ###

This source code is distrubuted under modified MIT License.

Main difference from MIT License that you MUST supply this original readme and list of authors of this Software with your source codes and/or compiled software.


Additionally you are invited to add authors as co-authors of your publication if you feel that this software made important influence to your work.
